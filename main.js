Array.min = function(array) {
    return Math.min.apply(Math, array);
};

var a = localStorage.getItem('fields');

if(a != null){
    a = JSON.parse(a)
}

var data = [];
var prize_draw = []
for (let i=0; i< 8; i++) {

    if(a != null){
        data.push({ id: '', type: 'question', color: a[i].color, text: a[i].name});
        prize_draw[i] = a[i].amount;
    }else{
        data.push({ id: '', type: 'question', color: '#FFF', text: 'Vazio'});
        prize_draw[i] = 0
    }
}


var b = prize_draw;

var soma = b.reduce(function(soma, i) {
    return soma + i;
});

let c = []
for (var i=0; i < b.length; i++){
   c[i] = Math.trunc((b[i] / Array.min(b)))
}

var qtd_p_ciclos = c.reduce(function(s, i) {
    return s + i;
});

var e = [];
for(let i =0; i < Math.trunc(soma / qtd_p_ciclos); i++){

    let f = []
    for(let i2 =0; i2<c.length; i2++){

        for(let i3 =0; i3<c[i2]; i3++){
            f.push(i2)
        }
    }

    e = e.concat(shuffleArray(f))
}

localStorage.setItem('e', JSON.stringify(e));

v = e;
let n = []
for(let i =0; i<b.length; i++){

    let index = v.indexOf(i);
    let count=0;
    while(index >= 0){
        count++;
        v.splice(index, 1);
        index = v.indexOf(i);
    }

    let res = (b[i] - count);

    if(res > 0){

        for(let j = 0; j < res; j++){
            n.push(i)
        }
    }
}

e = JSON.parse(localStorage.getItem('e'))
e = e.concat(shuffleArray(n))
var RouletteWheel = function (el, items) {
    this.$el = $(el);
    this.items = items || [];
    this._bis = false;
    this._angle = 0;
    this._index = 0;
    this.options = {
    angleOffset: -90 };

};

_.extend(RouletteWheel.prototype, Backbone.Events);

RouletteWheel.prototype.spin = function (_index) {

    var count = this.items.length;
    var delta = 360 / count;
    var index = !isNaN(parseInt(_index)) ? parseInt(_index) : parseInt(Math.random() * count);

    var a = index * delta + (this._bis ? 1440 : -1440);

    //a+=this.options.angleOffset;

    this._bis = !this._bis;
    this._angle = a;
    this._index = index;

    var $spinner = $(this.$el.find('.spinner'));

    var _onAnimationBegin = function () {
    this.$el.addClass('busy');
    this.trigger('spin:start', this);
    };

    var _onAnimationComplete = function () {
    this.$el.removeClass('busy');
    this.trigger('spin:end', this);
    };

    $spinner.
    velocity('stop').
    velocity({
    rotateZ: a + 'deg' },
    {
    //easing: [20, 7],
    //easing: [200, 20],
    easing: 'easeOutQuint',
    duration: 5000,
    begin: $.proxy(_onAnimationBegin, this),
    complete: $.proxy(_onAnimationComplete, this) });

};

RouletteWheel.prototype.render = function () {

    var $spinner = $(this.$el.find('.spinner'));
    var D = this.$el.width();
    var R = D * .5;

    var count = this.items.length;
    var delta = 360 / count;

    for (var i = 0; i < count; i++) {if (window.CP.shouldStopExecution(0)) break;

    var item = this.items[i];

    var color = item.color;
    var text = item.text;
    var ikon = item.ikon;

    var html = [];
    html.push('<div class="item" ');
    html.push('data-index="' + i + '" ');
    html.push('data-type="' + item.type + '" ');
    html.push('>');
    html.push('<span class="label">');
    if (ikon)
    html.push('<i class="material-icons">' + ikon + '</i>');
    html.push('<span class="text">' + text + '</span>');
    html.push('</span>');
    html.push('</div>');

    var $item = $(html.join(''));

    var borderTopWidth = D + D * 0.0025; //0.0025 extra :D
    var deltaInRadians = delta * Math.PI / 180;
    var borderRightWidth = D / (1 / Math.tan(deltaInRadians));

    var r = delta * (count - i) + this.options.angleOffset - delta * .5;

    $item.css({
        borderTopWidth: borderTopWidth,
        borderRightWidth: borderRightWidth,
        transform: 'scale(2) rotate(' + r + 'deg)',
        borderTopColor: color });


    var textHeight = parseInt(2 * Math.PI * R / count * .5);

    $item.find('.label').css({
        //transform: 'translateX('+ (textHeight) +'px) translateY('+  (-1 * R) +'px) rotateZ('+ (90 + delta*.5) +'deg)',
        transform: 'translateY(' + D * -.25 + 'px) translateX(' + textHeight * 1.03 + 'px) rotateZ(' + (90 + delta * .5) + 'deg)',
        height: textHeight + 'px',
        lineHeight: textHeight + 'px',
        textIndent: R * .1 + 'px' });


    $spinner.append($item);

    }window.CP.exitedLoop(0);

    $spinner.css({
    fontSize: parseInt(R * 0.06) + 'px' });


    //this.renderMarker();
};

RouletteWheel.prototype.renderMarker = function () {

    var $markers = $(this.$el.find('.markers'));
    var D = this.$el.width();
    var R = D * .5;

    var count = this.items.length;
    var delta = 360 / count;

    var borderTopWidth = D + D * 0.0025; //0.0025 extra :D
    var deltaInRadians = delta * Math.PI / 180;
    var borderRightWidth = D / (1 / Math.tan(deltaInRadians));

    var i = 0;
    var $markerA = $('<div class="marker">');
    var $markerB = $('<div class="marker">');

    var rA = delta * (count - i - 1) - delta * .5 + this.options.angleOffset;
    var rB = delta * (count - i + 1) - delta * .5 + this.options.angleOffset;

    $markerA.css({
    borderTopWidth: borderTopWidth,
    borderRightWidth: borderRightWidth,
    transform: 'scale(2) rotate(' + rA + 'deg)',
    borderTopColor: '#FFF' });

    $markerB.css({
    borderTopWidth: borderTopWidth,
    borderRightWidth: borderRightWidth,
    transform: 'scale(2) rotate(' + rB + 'deg)',
    borderTopColor: '#FFF' });

    $markers.append($markerA);
    $markers.append($markerB);

};

RouletteWheel.prototype.bindEvents = function () {
    //this.$el.find('').on('click', $.proxy(this.spin, this));
};

$('.button').on('click', function(){
    $('.button').prop("disabled", true);
    
    if((e != null) && (h = e[0] != null)){

        let h = e[0];
        spinner.spin(h)

        console.log('Key', h)

    }else{
        alert("Os prêmios acabaram!")
    }
})

var spinner;
$(window).ready(function () {
    spinner = new RouletteWheel($('.roulette'), data);
    spinner.render();
    spinner.bindEvents();

    spinner.on('spin:start', function (r) {console.log('spin start!');});
    spinner.on('spin:end', function (r) {

    if(a[r._index] != null){

        if(a[r._index].amount > 0){
            a[r._index].amount = a[r._index].amount - 1;
            e.splice(0, 1);
            localStorage.setItem('fields', JSON.stringify(a));
            $('.button').prop("disabled", false);
        }
    }

    var index = $('[data-index]');
        console.log(data);
    });

    let roda = document.querySelector('.roulette');
    roda.style.transform = 'scale(1.8)';
});
//# sourceURL=pen.js

function shuffleArray(arr) {

    for (let i = arr.length - 1; i > 0; i--) {

        const j = Math.floor(Math.random() * (i + 1));
        [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    return arr;
}